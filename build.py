from staticjinja import Site
import json
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument('--srcpath', dest='srcpath')
parser.add_argument('--outpath', dest='outpath')
parser.add_argument('--static', dest='staticpath')

args = parser.parse_args()

if __name__ == "__main__":
    with open('data.json') as f:
        data = json.load(f)
        site = Site.make_site(env_globals=data, staticpaths=[args.staticpath])

        site.outpath = args.outpath
        site.searchpath = args.srcpath

        site.render(use_reloader=False)
