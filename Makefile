# Makefile for NUSL-VYHLEDAVAC web theme

BASEDIR=$(shell pwd)
BUILD_SOURCE=templates
BUILD_TARGET=compiled
STATIC=static
VENV=venv
NAME=NUSL-VYHLEDAVAC
VERSION=0.1

.PHONY: all build-html create-venv

build-html: install-staticjinja
	@source ${BASEDIR}/${VENV}/bin/activate && python build.py \
		--srcpath=${BASEDIR}/${BUILD_SOURCE} \
		--outpath=${BASEDIR}/${BUILD_TARGET} \
		--static=${BASEDIR}/${BUILD_TARGET}/${STATIC}

create-venv:
	@python3 -m venv ${BASEDIR}/${VENV}

install-staticjinja: create-venv
	@source ${BASEDIR}/${VENV}/bin/activate && pip install -r ${BASEDIR}/requirements.txt

all:
	create-venv build-html
