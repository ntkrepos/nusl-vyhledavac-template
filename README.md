# NUŠL VYHLEDÁVAČ TEMPLATE

## Soubory a složky
- compiled/static/css/ - složka pro styly a vším s tím spojené (scss)
- compiled/static/css/sass - složka pro scss soubory
- compiled/static/css/compiled - výstupní složka pro sass; soubory v této složce se importují do html
- compiled/static/js/ - složka pro js a vším s tím spojené (prozatím prázdná)
- compiled/index.html - hlavní html šablona (s listem výsledků)
- compiled/front.html - úvodní html šablona ("google mode")
- Makefile - GNU Makefile s několika targety
- templates/ - složka šablon a jejich částí. Výsledné HTML je poněkud delší, proto
je rozděleno do kratších logických celků
- venv(vytvoří se po zavolání make create-venv) - venv složka
- sass_compile.sh - bash skript pro ruční kompilaci sass (watch režim)

## Technologie
- HTML 5 ~~(+ později jinja?)~~
- Jinja 2 - pro "složení" finální šablony
- Bootstrap pro responzivnost
- SASS pro stylování (lze použít syntax jako v běžném CSS)
- ~~Vue pro pro js (zatím nevyužito)~~ nadbytečné

### Makefile
Je vytvořen jednoduchý Makefile s několika targety:
- create-venv - vytvoří složku venv a samotný venv
- install-staticjinja - do vytvořeného venvu instaluje statisjinja balíček
- build-html - sestaví výsledné šablony pomocí staticjinja

Tímto cmd se vytvoří venv, nainstalují se balíčky v requirements.txt
a spustí se staticjinja -
```bash
make build-html
```

### SASS
Pokud není na vývojovém stroji instalován SASS, tak postačí v terminálu napsat (se sudo)

```bash
npm install -g sass
```
bez sudo
```bash
npm install sass
```
A pravděpodobně je i v systémovém repozitáři (přes dnf)

## Poznámky
Prozatím nejsou k dispozici reálná data ani static soubory (obrázky atd.), tudíž je to 
pouze nastylovaná šablona. CSS není pro lepší orientaci minifikováno. HTML vypadá zběsile, ale mělo
by se zkrátit použitím jinji ~~nebo Vue (procyklovat)~~.
Po důkladném a dlouhém testování bylo Vuejs zavrhnuto - pro projekt
tohoto rozsahu je nejspíše zbytečný a mohl by v budoucnu způsobit vícé problémů
než užitku.
Naopak pro skládání HTML je použita Jinja. Lze snadno upravovat určitý úsek
a pravděpodobně bude mnohem snažší nasazení do produkce (může se použít jak už
složená šablona, tak použít přímo jinja šablony, pokud bude zvolený framework
podporovat jinja šablony).

### Poznámky pro vývoj
~~Bude potřeba dohodnout co bude obstarávat Vue a co jinja samotná. V některých částech
se použití prolíná. Záleží i na způsobu poskytnutí dat, co bude neměnné nebo měněné
zřídka (obstará Vue), co bude generováno dynamicky (výsledky), obstarat jinjou.~~
Pro renderování šablony je použita jinja, jQuery pro javasript.
Doporučuji používat Makefile, odpadnou potíže s visrtuálním prostředím, instalací
balíčků a a celkové údržbě projektu. Samozřejmě lze kdykoli přidat další
funkcionalitu pokud bude potřeba. Prozatím umí pouze vytvářet venv, instalovat
balíčky a skládat šablony.

## Testování
Testováno a zkontrolováno důkladně v Chrome(v69) a lehce v FF(61). Vzhledem k použitým
technologiím se nemusí chovat zcela stabilně, obzvlášt ve starších verzích.
Nicméně se snažím použít co nejméně obskurností a co nejvíce vendor prefixů (-moz...)
Při úpravách je nutné dát **VELKÝ** pozor na chování v různých prohlížečích.

Odlišnosti od šablony - tlačítka v paginátoru jsou větší než jak je uvedeno. 
Trochu mi to protestovalo ohledně této velikosti a mobilního zobrazení, údajně je lepší
mít o něco větší velikost, aby člověk nemusel "klikat" na pár pixelů. Pokud by byl prostest lze
zmenšit.
