$(document).ready(function(){
    if (getCookie('style') == 'dark') {
        $(dark).attr('href', lightPath);      
    } else {
        $(light).attr('href', darkPath);
    }
});
