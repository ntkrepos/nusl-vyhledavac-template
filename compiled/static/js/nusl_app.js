function setDecollapsed(parent_li) {
    if (parent_li) {
            parent_li.getElementsByTagName('ul')[0].classList.toggle('decollapsed');
            parent_li.classList.toggle('arrow-down');
        }
}

function toggleClass() {
    var parent_li = this.parentElement;
    setDecollapsed(parent_li);
}

function collaps() {
    collapserf(true);
}
function decollaps() {
    collapserf(false);
}

function collapserf(collaps) {
    if (collaps === true) {

        var el2_class_condition = "collapsed";
        var el2_class = "";
    } else {
        el2_class_condition = "collapsed decollapsed";
        el2_class = "arrow-down";
    }
    var document_types = document.getElementsByClassName('document-types');
    Array.prototype.forEach.call(document_types, function (el, i) {
        let all_li = el.querySelector("ul:first-of-type").querySelectorAll("li");
        Array.prototype.forEach.call(all_li, function (el2, i) {

            if (el2.parentElement.className !== el2_class_condition) {
                el2.className = el2_class;
            }

            let first_li = el2.querySelector("ul:first-of-type");
            if (first_li) {
                first_li.className = el2_class_condition;
            }

        });

    });
}

$(".document-types > ul > li > span").each(function() {
    $( this ).on("click", toggleClass);
});

$("#collaps").on("click", collaps);
$("#decollaps").on("click", decollaps);

$('#dark').click(function (){
    $(dark).attr('href', lightPath);
    setCookie('style', 'dark', 90);
});
 $('#light').click(function (){
    $(light).attr('href', darkPath);
    setCookie('style', 'light', 90);
}); 
